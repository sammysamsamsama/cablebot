
import math
import asyncio
import moteus

velocity = 0.2
torque = 0.5
revCount = []


c1 = moteus.Controller(id=1)
c2 = moteus.Controller(id=2)
c3 = moteus.Controller(id=3)
c4 = moteus.Controller(id=4)
c5 = moteus.Controller(id=5)
c6 = moteus.Controller(id=6)
c7 = moteus.Controller(id=7)
c8 = moteus.Controller(id=8)


async def motorFunction(maxvel, maxt, revcount, c):
    # set_stop to restart moteus controller incase a fault occured
    # grap motor position data to display current postition before movement
    #await c.set_stop()
    state = await c.query()

    #print("c current position before:", state.values[moteus.Register.POSITION])

    # this if/else determines if we will use negative/positive velocity
    if (float(state.values[moteus.Register.POSITION]) < float(revcount)):
        maxvel = abs(float(maxvel))
    else:
        maxvel = -(float(maxvel))

    # movement of the motors and sleep so that the motor has time to move
    # before another position is given
    await c.set_position(position=float(state.values[moteus.Register.POSITION]), velocity=float(maxvel),
                         maximum_torque=float(maxt), stop_position=float(revcount), watchdog_timeout=math.nan)
    await asyncio.sleep(3)

    # grap motor position data to display current postition after movement
    state = await c.query()
    print("using t1 rev: ", revcount)
    print("c current position after:", state.values[moteus.Register.POSITION])
    #await c.set_stop()
    
zeros = [0.4,  # 1 +tighten
         -0.5, # 2 -tighten
         0.4,  # 3 +tighten
         -0.4, # 4 -tighten
         -0.4, # 5 +tighten
         -0.5, # 6 -tighten
         -0.2, # 7 -tighten
         0.6]  # 8 +tighten
async def main():
    await asyncio.sleep(1)
    
    task_t1 = asyncio.create_task(motorFunction(velocity, torque, zeros[0], c1))
    task_t2 = asyncio.create_task(motorFunction(velocity, torque, zeros[1], c2))
    task_t3 = asyncio.create_task(motorFunction(velocity, torque, zeros[2], c3))
    task_t4 = asyncio.create_task(motorFunction(velocity, torque, zeros[3], c4))
    task_t5 = asyncio.create_task(motorFunction(velocity, torque, zeros[4], c5))
    task_t6 = asyncio.create_task(motorFunction(velocity, torque, zeros[5], c6))
    task_t7 = asyncio.create_task(motorFunction(velocity, torque, zeros[6], c7))
    task_t8 = asyncio.create_task(motorFunction(velocity, torque, zeros[7], c8))
    await task_t1
    await task_t2
    await task_t3
    await task_t4
    await task_t5
    await task_t6
    await task_t7
    await task_t8
    
    
if __name__ == '__main__':
    asyncio.run(main())
