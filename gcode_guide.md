# Gcode Guide
This guide is a quick reference for gcode.

## Commands
G0 - Rapid motion to specified end point

G1 - Controlled motion to specified end point

G2 - Clockwise motion

G3 - Counterclockwise motion

G17 - x/y plane

G18 - z/x plane

G19 - y/z plane

G20 - Units set to inches

G21 - Units set to mm

G28 - Go back to home position

G90 - Absolute Mode (default mode); specified coordinate will move to exact location

G91 - Incremental Mode; specified coordinate will move to a location relative to the previous point

M30 - End of program

## Syntax
[**command**] [**coordinates**] [**offset**] [**feed rate**]

command - use commands from previous section (example: G1, G0)

coordinates - x, y, and z coordinates (example: X4 Y10 Z3)

offset - centerpoint offset for clockwise and counterclockwise motion (example: I2 J2)

feed rate - performs at certain speed; units in mm/min (example: F800)
