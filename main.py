# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 20:00:21 2021

@author: Josh Burkey
"""
import math


class point:
  def __init__(self, x, y,z):
    self.x = x
    self.y = y
    self.z = z

eo = []
mo = []
coords = []
maxvel = 0.0
maxt = 0.0
ccode = ''
'file reader, brings in file and operators and fills them'
def filereader(filename,maxvel,maxt,ccode,eo,mo,coords):
    with open(filename,"r") as fileo:
        count2 = 0
        newline = fileo.readlines()
        print("reading in preamble")
        for line in newline:
            count = 0
            'for each line strip the new line characters for each os'
            line = line.rstrip('\n')
            line = line.rstrip('\r')
            
        
            'split the coordinates into segments for processing'
            segment = line.split(",")
            
            'for each split part of the line strip off the comma and feed them in'
            x = 0
            y = 0
            z = 0
            for part in segment:
                if count2 == 0:
                    maxvel = part
                    count2+=1
                elif count2 == 1:
                    maxt = part
                    count2+=1
                elif count2 == 2:
                    ccode = part
                    count2 +=1
                elif count == 0 and 3 <= count2 < 11:
                    x = part    
                    count = count +1
                elif count == 1 and 3 <= count2 < 11:
                    y = part
                    count = count +1
                elif count == 2 and 3 <= count2 < 11:
                    z = part
                    position = point(int(x),int(y),int(z))
                    eo.append(position)
                    count2 +=1
                    print("reading in end effector offsets")
                elif count == 0 and 11 <= count2 < 19:
                    x = part    
                    count = count +1
                elif count == 1 and 11 <= count2 < 19:
                    y = part
                    count = count +1
                elif count == 2 and 11 <= count2 < 19:
                    z = part
                    position = point(int(x),int(y),int(z))
                    mo.append(position)
                    count2 +=1
                    print("reading in motor offsets")
                elif count == 0 and 19 <= count2:
                    x = part    
                    count = count +1
                elif count == 1 and 19 <= count2:
                    y = part
                    count = count +1
                elif count == 2 and 19 <= count2:
                    z = part
                    position = point(int(x),int(y),int(z))
                    coords.append(position)
                    count2 +=1
                    print("reading in coordinates")
                
    'close the file'    
    fileo.close()
    calcfunction(maxvel,maxt,ccode,eo,mo,coords)

'calculation function, inputs are control numbers and filled lists'
def calcfunction(maxvel,maxt,ccode,eo,mo,coords):
    'initialize the processing lists'
    mags = []
    prevmags = []
    newcoords = []
    revcount = []
    'we need a counter to move through the coordinate list at a different rate than the others'
    
    'we need to initialize prevmags at start'
    start = 0
    i = 0
    if start == 0:
        for i in range(8):
            magnitude = math.sqrt(((mo[i].x+eo[i].x)**2+(mo[i].y+eo[i].y)**2+(mo[i].z+eo[i].z)**2))
            prevmags.append(magnitude)        
            start +=1
    for count in range(len(coords)):
        'make new points based on the offsets'   
        for i in range(8):
            newpoint = point(eo[i].x+coords[count].x,eo[i].y+coords[count].y,eo[i].z+coords[count].z)
            newcoords.append(newpoint)
        #for i in range(4):
            #print("point t%d coord: %d %d %d"%(i,newcoords[i].x,newcoords[i].y,newcoords[i].z))
        #for i in range(4):
            #print("point b%d coord: %d %d %d"%(i+4,newcoords[i+4].x,newcoords[i+4].y,newcoords[i+4].z))
        'calculate the new magnitudes'
        for i in range(8):
            magnitude = math.sqrt((mo[i].x+newcoords[i].x)**2+(mo[i].y+newcoords[i].y)**2+(mo[i].z+newcoords[i].z)**2)
            mags.append(magnitude)
        'calculate rotations based on pi*radius divided by the magnitude change'
        for i in range(8):
            revcount.append((math.pi*50)/(mags[i]-prevmags[i]))
        
        
        'send the rotation count to the motor function'
        motorFunction(maxvel,maxt,revcount)
        're initialize everything by clearing the lists'
        newcoords.clear()
        prevmags.clear()
        revcount.clear()
        'mags is now the old mags'
        prevmags = mags.copy()
        mags.clear()
        
        'move on to the next point in the sequence'

        'if the control code is r repeat the coordinate loop else quit'
        if count == (len(coords)-1) and ccode == 'r':
            count == 0
            i=0
        elif count == (len(coords)-1) and ccode == 'q':
            print("demo ended")
        
    
        
    
'motor function takes in the rotation list and rotates the motors before returning'
'back to the control loop'
def motorFunction(maxvel,maxt,revcount):
    print("motors operating")
    for i in range(4):
        print("number of rotations for motor t%d : %f"%(i+1,revcount[i]))
    for i in range(4):
        print("number of rotations for motor b%d : %f"%(i+1,revcount[i+4]))
    return

filename = "demo.txt"
filereader(filename,maxvel,maxt,ccode,eo,mo,coords)     
