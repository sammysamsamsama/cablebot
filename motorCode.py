# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 20:00:21 2021

@author: Josh Burkey
"""
import math
import asyncio
import moteus
from initialZero import zeros


class point:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


eo = [point(20, 20, 20),
      point(20, -20, 20),
      point(-20, -20, 20),
      point(-20, 20, 20),
      point(20, 20, -20),
      point(20, -20, -20),
      point(-20, -20, -20),
      point(-20, 20, -20)
      ]

mo = [point(187,248,356),
      point(187,-248,356),
      point(-187,-248,356),
      point(-187,248,356),
      point(187,248,-356),
      point(187,-248,-356),
      point(-187,-248,-356),
      point(-187,248,-356)
      ]

coords = []
maxvel = 3.0
maxt = 4
ccode = ''
f = 0   #feed rate

c1 = moteus.Controller(id=1)
c2 = moteus.Controller(id=2)
c3 = moteus.Controller(id=3)
c4 = moteus.Controller(id=4)

c5 = moteus.Controller(id=5)
c6 = moteus.Controller(id=6)
c7 = moteus.Controller(id=7)
c8 = moteus.Controller(id=8)

'file reader, brings in file and operators and fills them'


async def filereader(filename, maxvel, maxt, ccode, eo, mo, coords):
    with open(filename, "r") as fileo:
        count2 = 0
        newline = fileo.readlines()
        'put x y z outside of read loop so they persist to next loop'
        x = 0
        y = 0
        z = 0
        
        for line in newline:
            'for each line strip the new line characters for each os'
            line = line.rstrip('\n')
            line = line.rstrip('\r')

            'split the coordinates into segments for processing'
            segment = line.split()
            print(segment)
            m30 = False
            'for each split part of the line strip space and feed them in'
            for part in segment:
                'only implemented G1 feed rate controlled move for now'
                if part == 'G1' or part == 'G01' or part == 'G0' or part == 'G00':
                    continue
                elif part == 'M30': #end program
                    m30 = True
                    break
                if 'X' in part:
                    x = int(part.lstrip('X'))
                if 'Y' in part:
                    y = int(part.lstrip('Y'))
                if 'Z' in part:
                    z = int(part.lstrip('Z'))
                if 'F' in part: # get feed rate if needed
                    # below line causes problem with fast motors/buzzing
                    #f = int(part.lstrip('F'))  
                    print("Feed rate:", f)
            if m30:
                break
            position = point(int(x), int(y), int(z))
            coords.append(position)
            print("reading in coordinates: ")
            print(position.x, position.y, position.z)
    'close the file'
    fileo.close()
    for i in range(3):
        await calcfunction(maxvel, maxt, ccode, eo, mo, coords)


'calculation function, inputs are control numbers and filled lists'

# TODO: add feed rate to parameters
async def calcfunction(maxvel, maxt, ccode, eo, mo, coords):
    'initialize the processing lists'
    mags = []
    ORIGINAL_MAGS = []
    prevmags = []
    newcoords = []
    revcount = []
    vels = []
    'we need a counter to move through the coordinate list at a different rate than the others'
    ''' 
    'ORIGINAL MAGS'
    start = 0
    i = 0
    if start == 0:
        for i in range(8):
            magnitude = math.sqrt(((mo[i].x + eo[i].x) ** 2 + (mo[i].y + eo[i].y) ** 2 + (mo[i].z + eo[i].z) ** 2))
            ORIGINAL_MAGS.append(magnitude)
            start += 1
    '''
    'we need to initialize prevmags at start'
    start = 0
    i = 0
    if start == 0:
        for i in range(8):
            magnitude = math.sqrt(((mo[i].x + eo[i].x) ** 2 + (mo[i].y + eo[i].y) ** 2 + (mo[i].z + eo[i].z) ** 2))
            prevmags.append(magnitude)
            start += 1
    for count in range(len(coords)):
        print("xyz", coords[count].x, coords[count].y, coords[count].z)
        'make new points based on the offsets'
        for i in range(8):
            newpoint = point(eo[i].x + coords[count].x, eo[i].y + coords[count].y, eo[i].z + coords[count].z)
            newcoords.append(newpoint)
        # for i in range(4):
        # print("point t%d coord: %d %d %d"%(i,newcoords[i].x,newcoords[i].y,newcoords[i].z))
        # for i in range(4):
        # print("point b%d coord: %d %d %d"%(i+4,newcoords[i+4].x,newcoords[i+4].y,newcoords[i+4].z))
        'calculate the new magnitudes'
        for i in range(8):
            magnitude = math.sqrt(
                (mo[i].x + newcoords[i].x) ** 2 + (mo[i].y + newcoords[i].y) ** 2 + (mo[i].z + newcoords[i].z) ** 2)
            mags.append(magnitude)
        max_mag_diff = 0
        'calculate rotations based on pi*radius divided by the magnitude change'
        for i in range(8):
            if abs(mags[i] - prevmags[i]) > 0:
                revcount.append((mags[i] - prevmags[i]) / (math.pi * 50))
            else:
                revcount.append(0)
            max_mag_diff = max(abs(mags[i] - prevmags[i]), max_mag_diff)
        'calculate vels based on magnitude change and feed rate'
        # TODO: calculate based on feedrate
        vels = []
        for i in range(8):
            if max_mag_diff > 0:
                vels.append((maxvel * abs(mags[i] - prevmags[i])/max_mag_diff)**1)
                #vels.append(maxvel)
            else:
                vels.append(0) 
            # vels.append(f * (mags[i] - prevmags[i]) / (max_mag_diff)) # 1105 = max possible magnitude difference
        'send the rotation count to the motor function'
        
        

        #task_t1 = asyncio.create_task(motorFunction(maxvel, maxt, revcount[0], c1))
        #await task_t1
        print("max_mag_diff", max_mag_diff)
        #for i in range(len(vels)):
            #print("{a:d} {b:1.3f} {c:1.3f} {d:1.3f}".format(a=i+1, b=vels[i], c=revcount[i], d=mags[i] - prevmags[i]))
            
            
        # if coords = (0,0,0) then go to true 0 position
        # altMotorFunction goes to stop position
        # motorFunction goes to current position + stop position
        if coords[count].x == 0 and coords[count].y == 0 and coords[count].z == 0:
            task_t1 = asyncio.create_task(altMotorFunction(maxvel, maxt, zeros[0], c1))
            task_t2 = asyncio.create_task(altMotorFunction(maxvel, maxt, zeros[1], c2))
            task_t3 = asyncio.create_task(altMotorFunction(maxvel, maxt, zeros[2], c3))
            task_t4 = asyncio.create_task(altMotorFunction(maxvel, maxt, zeros[3], c4))
            task_t5 = asyncio.create_task(altMotorFunction(maxvel, maxt, zeros[4], c5))
            task_t6 = asyncio.create_task(altMotorFunction(maxvel, maxt, zeros[5], c6))
            task_t7 = asyncio.create_task(altMotorFunction(maxvel, maxt, zeros[6], c7))
            task_t8 = asyncio.create_task(altMotorFunction(maxvel, maxt, zeros[7], c8))
            await task_t1
            await task_t2
            await task_t3
            await task_t4
            await task_t5
            await task_t6
            await task_t7
            await task_t8
        else:
            # top motors 3,4,7,8
            task_t3 = asyncio.create_task(motorFunction(vels[5], maxt, (-revcount[5]), c3))
            task_t4 = asyncio.create_task(motorFunction(vels[4], maxt, revcount[4], c4))
            task_t7 = asyncio.create_task(motorFunction(vels[6], maxt, revcount[6], c7))
            task_t8 = asyncio.create_task(motorFunction(vels[7], maxt, -revcount[7], c8))
            
            # bottom motors 1,2,5,6
            task_t1 = asyncio.create_task(motorFunction(vels[0], maxt, -revcount[0], c1))
            task_t2 = asyncio.create_task(motorFunction(vels[1], maxt, revcount[1], c2))
            task_t5 = asyncio.create_task(motorFunction(vels[2], maxt, -revcount[2], c5))
            task_t6 = asyncio.create_task(motorFunction(vels[3], maxt, revcount[3], c6))
            
            await task_t1
            await task_t2
            await task_t3
            await task_t4
            await task_t5
            await task_t6
            await task_t7
            await task_t8
            
        tdiv = 2
        rr = -0.05
        
        # reel in 1,2,5,6
        task_r1 = asyncio.create_task(motorFunction(vels[0], maxt/tdiv, -rr, c1))
        task_r2 = asyncio.create_task(motorFunction(vels[1], maxt/tdiv, rr, c2))
        task_r5 = asyncio.create_task(motorFunction(vels[2], maxt/tdiv, -rr, c5))
        task_r6 = asyncio.create_task(motorFunction(vels[3], maxt/tdiv, rr, c6))
        
        # reel in 3,4,7,8
        task_r3 = asyncio.create_task(motorFunction(vels[5], maxt/tdiv, -rr, c3))
        task_r4 = asyncio.create_task(motorFunction(vels[4], maxt/tdiv, rr, c4))
        task_r7 = asyncio.create_task(motorFunction(vels[6], maxt/tdiv, rr, c7))
        task_r8 = asyncio.create_task(motorFunction(vels[7], maxt/tdiv, -rr, c8))
        
        await task_r1
        await task_r2
        await task_r3
        await task_r4
        await task_r5
        await task_r6
        await task_r7
        await task_r8
        
        # top motors 3,4,7,8
        print("{a:d} {b:1.3f} {c:1.3f} {d:1.3f}".format(a=3, b=vels[4], c=-revcount[4], d=mags[4] - prevmags[4]))
        print("{a:d} {b:1.3f} {c:1.3f} {d:1.3f}".format(a=4, b=vels[5], c=revcount[5], d=mags[5] - prevmags[5]))
        print("{a:d} {b:1.3f} {c:1.3f} {d:1.3f}".format(a=7, b=vels[6], c=revcount[6], d=mags[6] - prevmags[6]))
        print("{a:d} {b:1.3f} {c:1.3f} {d:1.3f}\n\n".format(a=8, b=vels[7], c=-revcount[7], d=mags[7] - prevmags[7]))
        
        # bottom motors 1,2,5,6
        print("{a:d} {b:1.3f} {c:1.3f} {d:1.3f}".format(a=1, b=vels[0], c=-revcount[0], d=mags[0] - prevmags[0]))
        print("{a:d} {b:1.3f} {c:1.3f} {d:1.3f}".format(a=2, b=vels[1], c=revcount[1], d=mags[1] - prevmags[1]))
        print("{a:d} {b:1.3f} {c:1.3f} {d:1.3f}".format(a=5, b=vels[2], c=revcount[2], d=mags[2] - prevmags[2]))
        print("{a:d} {b:1.3f} {c:1.3f} {d:1.3f}".format(a=6, b=vels[3], c=-revcount[3], d=mags[3] - prevmags[3]))
        
        #print("c current position after:", state.values[moteus.Register.POSITION])
        
        
        're initialize everything by clearing the lists'
        newcoords.clear()
        prevmags.clear()
        revcount.clear()
        'mags is now the old mags'
        prevmags = mags.copy()
        mags.clear()

        'move on to the next point in the sequence'

        'if the control code is r repeat the coordinate loop else quit'
        if count == (len(coords) - 1) and ccode == 'r':
            count == 0
            i = 0
        elif count == (len(coords) - 1) and ccode == 'q':
            print("demo ended")


'motorFunction takes in max velocity, max torque, revolutions, and a moteus controller'


async def motorFunction(maxvel, maxt, revcount, c):
    # set_stop to restart moteus controller incase a fault occured
    # grap motor position data to display current postition before movement
    #await c.set_stop()
    state = await c.query()

   

    # this if/else determines if we will use negative/positive velocity
    if (float(state.values[moteus.Register.POSITION]) < float(revcount)):
        maxvel = abs(float(maxvel))
    else:
        maxvel = -(float(maxvel))

    # movement of the motors and sleep so that the motor has time to move
    # before another position is given
    await c.set_position(position=float(state.values[moteus.Register.POSITION]), velocity=float(maxvel),
                         maximum_torque=float(maxt), stop_position=round(float(state.values[moteus.Register.POSITION]),2) + float(revcount), watchdog_timeout=math.nan)
    await asyncio.sleep(0.4)

    print("c current rev position:", state.values[moteus.Register.POSITION])
    # grap motor position data to display current postition after movement
    #state = await c.query()
    #print("using t1 rev: ", revcount)
    #print("c current position after:", state.values[moteus.Register.POSITION])
    #print()
    #await c.set_stop()

async def altMotorFunction(maxvel, maxt, revcount, c):
    # set_stop to restart moteus controller incase a fault occured
    # grap motor position data to display current postition before movement
    #await c.set_stop()
    state = await c.query()

    # this if/else determines if we will use negative/positive velocity
    if (float(state.values[moteus.Register.POSITION]) < float(revcount)):
        maxvel = abs(float(maxvel))
    else:
        maxvel = -(float(maxvel))

    # movement of the motors and sleep so that the motor has time to move
    # before another position is given
    await c.set_position(position=float(state.values[moteus.Register.POSITION]), velocity=float(maxvel),
                         maximum_torque=float(maxt), stop_position=float(revcount), watchdog_timeout=math.nan)
    await asyncio.sleep(0.4)

    print("c current rev position:", state.values[moteus.Register.POSITION])
    # grap motor position data to display current postition after movement
    #state = await c.query()
    #print("using t1 rev: ", revcount)
    #print("c current position after:", state.values[moteus.Register.POSITION])
    #print()
    #await c.set_stop()

filename = "gcode_axis_testing.txt" #change txt file if needed!
asyncio.run(filereader(filename, maxvel, maxt, ccode, eo, mo, coords))


