# Cablebot
# Road map for development:
Conceptually the general purpose program is divided into 3 parts

# part 1 the loader function
this function first loads the preamble into its variables and then loads the list of points the end effector is to move to this should be a standard text file where points are individual lines and follow the standard x,y,z

NOTE SOME PARAMETERS HAVE BEEN CHANGED

the preamble should be structured as follows
diameter of spindle
max velocity
max torque
offset dimensions of end effector points (in our case it will be (20,20,20) with sign changes for the various quadrants) of which there are 8
loophole positions of which there are 8
they will be loaded in as te1, te2, te3 etc for the points corrisponding to the connection points on the end effector that are also connected to the motors mounted at the top of the workcell and be1,be2,be3 for those connected to the bottom motors
the motor loophole positions will follow a similar format tm1,tm2,tm3,tm4,bm1,bm2,bm3,bm4

after the preamble is loaded the rest of the text file is loaded into a list structure (may revise this if it becomes cumbersome to implement) as a set of vectors until the last line which should contain a control character which is the last element in the array.

control characters for this semester will just either be l for loop or q for quit, loop resets the iterator for the list and reruns the movement program from the first coordinate after it started. q moves the end effector back to 0,0,0, and closes the program.

# part 2 the calculation function
for each step the movement vector is loaded as newvec with the original vector being oldvec, the lengths of all 8 cables is also tracked, the new length is calculated as an offset to each corner from the newly loaded point, and is a line between that offset and the corresponding loophole
the starting point line magnitudes will be recorded and saved as startmag1, startmag2 etc and newmag1, newmag2, etc for each of the 8 lines
magnitude is calculated as sqrt((x2-x1)**2+(y2-y1)**2+(z2-z1)**2) 

alternately let p2 be the vector for the loophole, and p1 be the new vector for the point on the end effector
np.linalg.norm(p2-p1) should work.

once the new lines magnitude is calculated it is subtracted from the previous line this is done for all 8 lines (newmagnitude - oldmagnitude)

# part 3 the motor function
the difference in magnitude is converted into rotations based on the diameter of the spindle
this number of rotations is then prepared to send to the corresponding motor controller and sent to run the step. for testing this difference in magnitude will be printed out for each motor.
positive is reel out, negative is reel in

after the motors run, repeat the loop from the next part of the list, if the control letter is in that position run it, again l resets i, q stops the program after return to 0,0,0,
also include a keyboard input for q for interrupt which prints out last position allowing reset to 0,0,0, to run again

